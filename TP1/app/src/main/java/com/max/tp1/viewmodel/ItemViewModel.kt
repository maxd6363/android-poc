package com.max.tp1.viewmodel

import androidx.lifecycle.ViewModel
import com.max.tp1.model.Item
import java.util.LinkedList

class ItemViewModel : ViewModel() {
    val items : MutableList<Item> = LinkedList<Item>()
}